# Generated by Django 2.2.1 on 2019-06-07 07:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Columns',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Tasks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('task', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='TasksColumns',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('column', models.ForeignKey(default=6, on_delete=django.db.models.deletion.CASCADE, related_name='postIts', to='tasks.Columns')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='status', to='tasks.Tasks')),
            ],
        ),
    ]
