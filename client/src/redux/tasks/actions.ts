import {  IPostIt } from "./state";

type FAILED = "TASK_CREATE_FAILED" | "TASK_UPDATE_FAILED" | "TASKS_LOAD_FAILED"

export const tasksLoadSuccess=(postIts:IPostIt[])=>{
    return{
        type: "TASKS_LOAD_SUCCESS" as "TASKS_LOAD_SUCCESS",
        postIts
    }
}

export const taskCreateSuccess= (postIt:IPostIt)=>{
    return {
        type: "TASK_CREATE_SUCCESS" as "TASK_CREATE_SUCCESS",
        postIt
    }
}

export const taskUpdateSuccess =(postIt:IPostIt)=>{
    return{
        type: "TASK_UPDATE_SUCCESS" as "TASK_UPDATE_SUCCESS",
        postIt
    }
}

export const taskSelectedAndDrop = (taskId: string, statusId: string)=>{
    return{
        type: "TASK_SELECTED_AND_DROP" as "TASK_SELECTED_AND_DROP",
        taskId,
        statusId
    }
}


export const taskDeleteSuccess =(msg:string)=>{
    return{
        type: "TASK_DELETE_SUCCESS" as "TASK_DELETE_SUCCESS",
        msg
    }
}

export const taskActionFailed=(type:FAILED, msg:string) =>{
    return{
        type,
        msg
    }
}

type taskActionCreator= typeof taskCreateSuccess| 
                        typeof taskUpdateSuccess| 
                        typeof tasksLoadSuccess|
                        typeof taskDeleteSuccess|
                        typeof taskActionFailed |
                        typeof taskSelectedAndDrop
                        

export type ITaskAction = ReturnType<taskActionCreator>