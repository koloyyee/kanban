import {IColumn } from "./state";
import { DraggableLocation } from "react-beautiful-dnd";

export const loadColumnsSuccess=( columns:IColumn[])=>{
    return{
        type: "LOAD_COLUMN_SUCCESS" as "LOAD_COLUMN_SUCCESS",
        columns
    }
}

export const moveBetweenColumns=(start:DraggableLocation, finish:DraggableLocation, taskId:string)=>{
    return{
        type: "MOVE_SUCCEED" as "MOVE_SUCCEED",
        start,
        finish,
        taskId
    }
}

export const columnsFailedAction=(msg:string)=>{
    return{
        type: "COLUMN_FAILED_ACTION" as "COLUMN_FAILED_ACTION" ,
        msg
    }
}

type columnsActionCreator = typeof loadColumnsSuccess| typeof columnsFailedAction | typeof moveBetweenColumns;
export type IColumnAction = ReturnType<columnsActionCreator>