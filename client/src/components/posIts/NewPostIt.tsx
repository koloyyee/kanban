import * as React from 'react';
import { Button, InputGroup, InputGroupAddon, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { createTask } from '../../redux/tasks/thunk';
import { connect } from 'react-redux';
import './NewPostIt.scss'

interface INewTaskState {
    modal: boolean,
    name: string,
    task: string,
}

interface INewTaskProps {
    createTask: (
        name: string,
        task: string,
    ) => void,
}

export class NewTask extends React.Component<INewTaskProps, INewTaskState>{

    constructor(props: INewTaskProps) {
        super(props)
        this.state = {
            modal: false,
            name: '',
            task: '',
        }
    }

    private toggle = () => (
        this.setState({ modal: !this.state.modal })
    )
    private submit = () => {
        this.props.createTask(this.state.name, this.state.task)
        this.setState({
            name: '',
            task: '',
            modal: false,
        })
    }

    private reset = () => {
        this.setState({
            name: '',
            task: '',
            modal: false,
        })
    }

    private textInput = (field: "name" | "task", e: React.ChangeEvent<HTMLInputElement>) => {
        const state = {} as INewTaskState
        state[field] = e.target.value
        this.setState(state)
    }
    public render() {
        return (
            <div >
                <Button className="new-task" color="primary" onClick={this.toggle}>{this.state.modal ? "Close" : "New Task"}</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} >
                    <ModalHeader toggle={this.toggle}>Create a New Task</ModalHeader>
                    <ModalBody>
                        <InputGroup>
                            <InputGroupAddon addonType="append">
                            </InputGroupAddon>
                            <input value={this.state.name} onChange={this.textInput.bind(this, 'name')} name="name" placeholder="Your name" type="text" />
                            <input value={this.state.task} onChange={this.textInput.bind(this, 'task')} name="task" placeholder="Task" type="text" />
                            
                        </InputGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.submit}>Submit</Button>{' '}
                        <Button color="secondary" onClick={this.reset}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>

        )
    }
}

const mapDispatchToProps = { createTask }
export  default connect(null, mapDispatchToProps)(NewTask)