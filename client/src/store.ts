import {combineReducers, compose, createStore, applyMiddleware} from 'redux';
import thunk, { ThunkAction } from 'redux-thunk'
import { ITaskAction } from './redux/tasks/actions';
import { IColumnState } from './redux/columns/state';
import { columnReducer } from './redux/columns/reducers';
import { IColumnAction } from './redux/columns/actions';

declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export interface IRootState{
    allColumns:IColumnState
}

export type IRootAction = ITaskAction |IColumnAction

const rootReducer = combineReducers<IRootState>({
    allColumns:columnReducer
})

export type ThunkResult <R> = ThunkAction<R, IRootState, null, IRootAction>

export default createStore<IRootState,IRootAction,{},{}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(thunk)
    )
)