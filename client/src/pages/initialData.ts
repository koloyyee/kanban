export const initialData = [
    {
        id:20,
        status:"To-Do",
        postIts:[
            
        ]

    },
    {
        id:21,
        status:"Doing",
        postIts:[
            
        ]

    },
    {
        id:22,
        status:"Complete",
        postIts:[]

    },
    {
        id:23,
        status:"QA",
        postIts:[]

    },
    {
        id:24,
        status:"Completed",
        postIts:[]

    },{
        id:25,
        status:"Backlog",
        postIts:[
            {
                id:1,
                name:"David",
                task:"Research"

            },
            {
                id:2,
                name:"Pete",
                task:"SEO"

            },
            {
                id:3,
                name:"Queenie",
                task:"Cold-call"

            },
            {
                id:4,
                name:"David",
                task:"React"

            },
            {
                id:5,
                name:"Mark",
                task:"Django"

            }

        ]

    },
]