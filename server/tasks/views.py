from tasks.models import Tasks
from tasks.serializers import TasksSerializer
from rest_framework import generics
from django.shortcuts import get_object_or_404
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from tasks.serializers import ColumnsSerializer , TasksColumnsSerializer
from tasks.models import Columns, TasksColumns
from rest_framework import viewsets

#for JSON file from redux-thunk
# parser_classes = (JSONParser,)

class ColumnsViews(viewsets.ModelViewSet):
    queryset = Columns.objects.all()
    serializer_class = ColumnsSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

class TasksColumnsViews(viewsets.ModelViewSet):
    queryset = TasksColumns.objects.all()
    serializer_class = TasksColumnsSerializer

class TasksViews(viewsets.ModelViewSet):
    queryset = Tasks.objects.all()
    parser_classes = (FormParser, JSONParser, MultiPartParser)
    serializer_class = TasksSerializer


# class TasksCreate(generics.CreateAPIView):
#     queryset = Tasks.objects.all()
#     newTask = Tasks.objects.all()[len(Tasks.objects.all())-1]
#     lookup_field= ['name', 'task']
#     serializer_class = TasksSerializer

#     def get_object(self):
#         data = self.kwargs ['name', 'task']
#         return get_object_or_404(Tasks, data=['name', 'task'])

#     parser_classes = (JSONParser,)
#     def post(self, request, *args, **kwargs):
#         return TasksColumns.objects.create(task_id = newTask.id)
#         return self.create(request, *args, **kwargs)


# class TasksUpdate(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Tasks.objects.all()
#     lookup_field= 'id'
#     serializer_class = TasksSerializer
    
#     def get_object(self):
#         id = self.kwargs ['id']
#         return get_object_or_404(Tasks, id=id)

#     parser_classes = (JSONParser,)
#     def put(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)

class TasksDelete(generics.DestroyAPIView):
    queryset= Tasks.objects.all()
    lookup_field='id'
    serializer_class= TasksSerializer

    def get_object(self):
        id = self.kwargs ['id']
        return get_object_or_404(Tasks, id=id)

    def delete(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


