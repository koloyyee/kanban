import { ThunkResult } from "../../store";
import { Dispatch } from "redux";
import { ITaskAction, tasksLoadSuccess, taskActionFailed, taskCreateSuccess, taskUpdateSuccess, taskDeleteSuccess } from "./actions";
import { IPostIt } from "./state";

const {REACT_APP_API_SERVER} = process.env

export const loadTasks=():ThunkResult<void>=>{
    return async (dispatch:Dispatch<ITaskAction>)=>{
        const res = await fetch (`${REACT_APP_API_SERVER}/tasks`,{
            method:"GET"
        })
        const result:IPostIt[] = await res.json()
        if(res.status===200 && result){
            dispatch(tasksLoadSuccess(result))
        }else{
            dispatch(taskActionFailed('TASKS_LOAD_FAILED', 'failed to load'))
        }
    }
}

export const createTask=(name:string, task:string):ThunkResult<void>=>{
    const data = {name,task}
    return async (dispatch:Dispatch<ITaskAction>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/tasks/`,{
            method:"POST",
            headers: {
                "Content-Type" :"application/json",
            },
            body: JSON.stringify(data)
        })

        const result:IPostIt = await res.json()
        if(res.status === 201){
            dispatch(taskCreateSuccess(result))
        }else {
            dispatch(taskActionFailed('TASK_CREATE_FAILED', 'Unable to Create Task'))
        }
    }
}


export const updateTask=(id:number,task:string):ThunkResult<void>=>{
    const data = {id, task}
    return async (dispatch:Dispatch<ITaskAction>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/tasks/update/${id}`,{
            method:"PUT",
            headers: {"Content" :"application/json"},
            body: JSON.stringify(data)
        })

        const result:IPostIt = await res.json()
        if(res.status === 200){
            dispatch(taskUpdateSuccess(result))
            
        }else {
            dispatch(taskActionFailed('TASK_UPDATE_FAILED', 'Unable to Update Task'))
        }
    }
}

export const deleteTask=(id:number):ThunkResult<void>=>{
    return async (dispatch:Dispatch<ITaskAction>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/tasks/delete/${id}`,{
            method:"DELETE",
        })

        await res.json()
        if(res.status === 200){
            dispatch(taskDeleteSuccess('deleted'))
        }else {
            dispatch(taskActionFailed('TASK_UPDATE_FAILED', 'Unable to delete Task'))
        }
    }
}