from rest_framework import serializers
from tasks.models import Tasks, Columns, TasksColumns

class TasksSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tasks
        fields = ('id', 'name', 'task')

    def create(self, validated_data):
        tasks = Tasks.objects.create(**validated_data)
        task_id = Tasks.objects.all()[len(Tasks.objects.all())-1].id
        TasksColumns.objects.create(task_id=task_id)
        return tasks


class ColumnsSerializer(serializers.ModelSerializer):
    class Meta:
        model= Columns
        fields =('id', 'url', 'status', 'postIts')
        depth=2

class TasksColumnsSerializer(serializers.ModelSerializer):
    class Meta:
        model= TasksColumns
        fields =('id', 'column','task')
        depth= 1