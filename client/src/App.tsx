import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import  Kanban  from './pages/Kanban';

const App: React.FC = () => {
  return (
    <div className="App">
      <Provider store={store}>
            <Kanban  key='kanban'/>
      </Provider>

    </div>

  );
}

export default App;
