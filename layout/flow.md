According to the requirements, first we have to setup a React as frontend and Django as 
backend. 

For frontend:

1: CRUD of tasks
2: Develop columns from "To-Do", "Doing", "Completed", "QA", "Closed", "Backlog"
3: Drag and Drop
4: fetch with RESTful API 
	- componentDidMount load all 4 columns' history(get)
	- componentDidUpdate render component changes (post/put/delete)
5: css, hopefully LESS or SCSS


For backend:

1: handle RESTful API
2: connect between frontend and database


For database:

1: setup MySQL db
2: create tables for tasks