import { ThunkResult } from "../../store";
import { Dispatch } from "redux";
import { IColumnAction, loadColumnsSuccess, columnsFailedAction, moveBetweenColumns,  } from "./actions";
import { IColumn } from "./state";
import { DraggableLocation } from "react-beautiful-dnd";

const {REACT_APP_API_SERVER} = process.env

export const loadColumns=():ThunkResult<void>=>{
    return async(dispatch:Dispatch<IColumnAction>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/columns`,{
            method:"GET"
        })
        const result:IColumn[] = await res.json()
        if(res.status===200 && result){
            dispatch(loadColumnsSuccess(result))
        } else {
            dispatch(columnsFailedAction('Unable to load'))
        }
    }
}

export const moveBetweenCols=(start:DraggableLocation, finish:DraggableLocation, taskId:string):ThunkResult<void>=>{
    const data = {taskId,start,finish}
    return async (dispatch:Dispatch<IColumnAction>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/tasks/action/${taskId}`, {
            method:"PUT",
            headers:{"Content-type": "application/json"},
            body: JSON.stringify(data)
        })
        const result= await res.json()
        if(res.status=== 200 && result){
            dispatch(moveBetweenColumns(result.start, result.finish, result.postIts.taskId))
        } else {
            dispatch(columnsFailedAction('Action Failed'))
        }
    }
}