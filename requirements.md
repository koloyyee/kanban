Assessment Description

The assessment contains three parts, and with one html file attached. 
It is a Kanban board contains five columns and each column is representing the different status of a task.
You are about to revamp the Kanban board base on the given HTML file.
You are allowed to use any CSS framework and javascript library that you think would help on this assessment.
You are required to submit this assessment on a GitHub or similar version control systems.

Part One
Please look into the HTML code and restructure the HTML file which should meet current industrial standard.
You are required to improve the overall design of the Kanban board by using the cascading style sheets or CSS preprocessor of your choice.
Currently, this Kanban board does not look proper on a mobile device please improve the overall look and make the Kanban board responsive.

 Part Two
By default, there are two tasks contained set within the HTML file at the bottom of the Kanban called Backlog area.
Please implement a drag and drop feature on each task which allows a user to move a task to the different status of the Kanban board.
Please provide a Create Task Function/Form in the Kanban initially, every task created should be displayed on the Backlog area at first.

Part Three
Each created task's content and status should be stored within a database. You are required to use the Django web framework for this.
You are required to develop your own backend functions and views to support the point above.