export interface IPostIt{
    id:number,
    name:string,
    task:string,
}
export interface IPostItState{
    postIts:IPostIt[]
}