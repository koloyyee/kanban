import * as React from 'react';
import './PostIt.scss'
import { Draggable, DraggableProvided } from 'react-beautiful-dnd';
import { Button } from 'reactstrap';
import {deleteTask} from '../../redux/tasks/thunk'
import { connect } from 'react-redux';


interface ITaskProps {
  id: number,
  posIt: any
} 


export const PostIt: React.FunctionComponent<ITaskProps> = ({ id, posIt }) => {

  // const delTask =(id:number, event:React.MouseEvent)=>{
  //   eval(event.currentTarget.).bind(this)(id,event)
  // }

  return (
        <div className ="post-it">
          <Draggable draggableId={id.toString()} index={id}>
            {(provided: DraggableProvided) => (

              <div
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                className="post-it"
              >
                    <span className="creator"> {posIt.task.name} </span>
                    <p className="task"> {posIt.task.task}</p>
                    <Button color="danger" >Del</Button>
              </div>
            )}
          </Draggable>
        </div>

  )
}

// const mapDispatchToProps={ deleteTask}
// export default connect(null, mapDispatchToProps)(PostIt)