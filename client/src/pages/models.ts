import { IColumn } from "../redux/columns/state";
import { IPostIt } from "../redux/tasks/state";

export interface IKanBanState{
    column:IColumn,
    task:IPostIt
}