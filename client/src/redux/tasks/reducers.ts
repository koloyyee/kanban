import { ITaskAction, taskSelectedAndDrop } from "./actions";
import { IPostItState } from "./state";

const initialState={
    postIts:[{
        id:0,
        name:"",
        task:""
    }]
}


export  const taskReducer = (state:IPostItState = initialState, action:ITaskAction):IPostItState =>{
    switch(action.type){
            case 'TASKS_LOAD_SUCCESS':
                return{
                    ...state,
                    postIts:action.postIts
                }
            case 'TASK_UPDATE_SUCCESS':
       
            case 'TASK_CREATE_SUCCESS':
            return{...state, 
                postIts:[action.postIt]
            }
        case 'TASKS_LOAD_FAILED':
        case 'TASK_CREATE_FAILED':
        case 'TASK_UPDATE_FAILED':
            return {
                ...state,
            }
        default:
            return state   
    }
}