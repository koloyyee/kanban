from django.db import models


class Tasks (models.Model):
    name = models.CharField(max_length=100)
    task = models.CharField(max_length=250)

    def __str__(self):
        return self.task

class Columns (models.Model):
    status = models.CharField(max_length=10, )

    def __str__(self):
        return self.status


class TasksColumns(models.Model):
    column= models.ForeignKey('Columns', related_name='postIts', on_delete=models.CASCADE, default=6)
    task= models.ForeignKey('Tasks', related_name='status', on_delete=models.CASCADE, )
    