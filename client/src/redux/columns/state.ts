import { IPostIt } from "../tasks/state";

export interface IColumn{
    id:number,
    url:string,
    status:string,
    postIts:IPostIt[]
}

export interface IColumnState{
    columns: IColumn[]
}

