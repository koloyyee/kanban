import { IColumnState } from "./state";
import { IColumnAction } from "./actions";


const initialState ={
    columns: [],
}

export const columnReducer =(state:IColumnState = initialState, action: IColumnAction):IColumnState=>{
    switch(action.type){
        case 'LOAD_COLUMN_SUCCESS':
                return{
                    ...state,
                    columns:action.columns
                }
        // case 'TASK_CREATE_SUCCESS':

        //     const cols = state.columns.slice();
        //     const postIts =  cols[cols.length - 1].postIts.slice();

        //     const newPostITs = postIts.concat({ 
        //         id: action.postIt.id, 
        //         task: { name: action.postIt.name, task: action.postIt.task });
        //     cols[cols.length - 1].postIts = newPostITs;

        //     return {
        //             ...state,
        //             columns: cols
        //         }
        case 'MOVE_SUCCEED':

            const {start, finish, taskId} = action;
            // make a copy of the column, because they should be immutable
            const clonedColumns = state.columns.slice();
            // find the start column and finish column
            const startColumn = clonedColumns.find(task=> task.id.toString() === start.droppableId);
            const finishColumn = clonedColumns.find(task=>task.id.toString()=== finish.droppableId);
            
            if(!startColumn || !finishColumn){
                return state;
            }
            // again, because of principle of immutable
            const clonedStartPostIts = startColumn.postIts.slice()
            // set the pointer
            const targetPostIt = clonedStartPostIts.find(postIt=> postIt.id.toString() === taskId)

            if(!targetPostIt){
                return state;
            }
            // Remove Post It
            const remainingPostIts = clonedStartPostIts.filter(({id})=> id.toString()!==taskId)
            // Add Post It
            const clonedFinishPostIts = finishColumn.postIts.slice()
            clonedFinishPostIts.splice(finish.index,0,targetPostIt)
            //Update Column
            const columns = clonedColumns.map(column=>{
                if(column.id.toString() === finish.droppableId){
                    return {...column, postIts: clonedFinishPostIts}
                }
                if(column.id.toString() === start.droppableId){
                    return {...column, postIts: remainingPostIts}
                }
                return column
            })

            return{
                ...state,
                columns

            }
        case 'COLUMN_FAILED_ACTION':
            return{
                ...state,
            }
        default:
            return state
    }
}