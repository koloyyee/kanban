import * as React from 'react';
import './Columns.scss'
import { Card, CardTitle, Container, Col, Row} from 'reactstrap';
import { Droppable, DroppableProvided,  } from 'react-beautiful-dnd';
import { IPostIt } from '../../redux/tasks/state';
import { PostIt } from '../posIts/PostIt';


interface IColumnsProps {
    id:number,
    postIts:IPostIt[]
    outline: string,
    boardName: string,
}

export class Columns extends React.Component<IColumnsProps, {}>{
    public render() {

        const {id, postIts, boardName, outline}= this.props;
        return (
            <div className = "board" >
                <Droppable key={id} droppableId={id.toString()}>
                    {(provided:DroppableProvided)=>(
                        <div 
                        ref={provided.innerRef}    
                        {...provided.droppableProps}
                        >
                            <Row>
                                <Card body inverse color={outline} className={boardName === 'Backlog'? "backlog" : "board"} >
                                        <CardTitle className="title">{boardName}</CardTitle>
                                        {postIts.map((mission=><div>
                                            <Col xs="12"  sm= '10' md ="10" lg='3' >
                                                <PostIt  id={mission.id} posIt={mission}/>
                                                </Col>
                                            </div>))}
                                </Card>
                            </Row>
                        
                    {provided.placeholder}
                        </div>
                        )}
                </Droppable>
        </div>
        )
    }
}
