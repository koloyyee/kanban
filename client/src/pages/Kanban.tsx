import * as React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { IRootState } from '../store';
import { connect } from 'react-redux';
import { DragDropContext, DropResult, DraggableLocation } from 'react-beautiful-dnd';
import {  Columns } from '../components/columns/Columns';
import { IColumn } from '../redux/columns/state';
// import { initialData } from './initialData';
import {  moveBetweenColumns } from '../redux/columns/actions';
import NewPostIt from '../components/posIts/NewPostIt';
import { loadTasks } from '../redux/tasks/thunk';
import { loadColumns } from '../redux/columns/thunk';

interface KanbanProps{
    columns:IColumn[]
    loadColumns:()=>void,
    moveBetweenColumns:(start: DraggableLocation, finish:DraggableLocation, taskId: string)=>void
}


export class Kanban extends React.Component<KanbanProps,{}>{
    
    public  componentDidMount(){
         this.props.loadColumns()
    }
    
    private outline=(status:string)=>{
        switch(status){
            case("To-Do"):
                return "danger"
            case("Doing"):
                return "primary"
            case("Completed"):
                return "success"
            case("QA"):
                return "warning"
            case("Closed"):
                return "secondary"
            default:
                return "";
        }
    }

    
    onDragEnd =(result:DropResult) =>{

        const { destination, source, draggableId } = result;

        if (!destination ||
            (destination.droppableId === source.droppableId && destination.index === source.index)) {
            return;
        }

        if (destination.droppableId === source.droppableId && destination.index !== source.index) {
            // Todo: move card in the same workflow
        } else {
            this.props.moveBetweenColumns(source, destination, draggableId)
        }

        
        // task update

    }
    public render(){
        return(
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <Container>
                        <Row>
                            <NewPostIt />
                            {this.props.columns.map((column,i) =>{
                                return <Col sm ="12" md="6" lg="auto" xs="12" key={i}>
                                    <Columns key={i} id={column.id} boardName={column.status} outline={this.outline(column.status)} postIts={column.postIts}/></Col>
                            })}
                        </Row>
                    </Container>
                </DragDropContext>
        )
    }
}
const mapStateToProps=(state:IRootState)=>{
    const columns = state.allColumns.columns
    return {columns}
}
const mapDispatchToProps={loadColumns, moveBetweenColumns }
export default connect(mapStateToProps,mapDispatchToProps)(Kanban)