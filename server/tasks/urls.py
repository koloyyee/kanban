from django.urls import path
from . import views

# urlpatterns= [
#     path('tasks/', views.TasksViews.as_view()),
#     path('tasks/post', views.TasksCreate.as_view()),
#     path('tasks/update/<int:id>/', views.TasksUpdate.as_view()),
#     path('tasks/delete/<int:id>/', views.TasksDelete.as_view()),
#     path('columns', views.ColumnsViews.as_view()),
#     path('tasks_columns', views.TasksColumnsViews)
# ]